var HomePage = require('./pages/rocketmiles-page');

describe('rocketmiles homepage', function() {

  var homepage = new HomePage();

  beforeEach(function() {
    homepage.get();
  });

  it('should have a title', function() {

    // browser.get('https://angularjs.org/'); // functioning site with angular
    // browser.explore(); // REPL

    expect(homepage.title).toEqual('Rocketmiles - Book Hotels Earn Thousands of Frequent Flyer Miles'); 
  });
});