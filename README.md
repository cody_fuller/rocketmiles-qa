# Rocketmiles QA Engineer Coding Challenge

### Prompt
Prerequisite:

Tools - Protractor

Language - Your choice

Website - rocketmiles.com

**Part 1:**

Using protractor as an automation framework write as many tests as you think would be sufficient to test the search form (destination, check-in, checkout, # of adults, # of rooms, submit button) on the homepage of rocketmiles.com website. If you feel adventurous add any other tests that you might think could be useful for the homepage.

**Part 2:**

Go through the flow of our site, making certain assumptions regarding the functionality of our site and come up with scenarios of what you think should be tested. Just write down scenarios that you would manually test or that you would want to automate.

## Part 1

### Prereqs

* Selenium server running at `http://localhost:4444`
* Node v6 because protractor has issues with debugging in v8. So we go back to the latest LTS

### Install
`npm install`

### Running
Assuming the selenium server is running, enter the following command to start the tests.

`npm test`

### Issues!

I'm hitting a roadblock due to a FB plugin that rocketmiles is using on the homepage. I'm lead to believe after some research that it's due to an async/promises issue with how FB is retrieving data. I've tested my simple code on other angular pages and haven't been able to recreate it on anything but rocketmiles' site.

As seen in the following terminal output, I discovered this issue by using the interactive REPL provided by protractor and then attempting to retrieve data from the page.

```
➜  rocketmiles-qa (master) ✗ npm test

> rocketmiles-qa@1.0.0 test /Users/cody/Code/rocketmiles-qa
> protractor conf.js

[23:33:57] I/launcher - Running 1 instances of WebDriver
[23:33:57] I/hosted - Using the selenium server at http://localhost:4444/wd/hub
Started
[23:34:00] I/protractor -
[23:34:00] I/protractor - ------- Element Explorer -------
[23:34:00] I/protractor - Starting WebDriver debugger in a child process. Element Explorer is still beta, please report issues at github.com/angular/protractor
[23:34:00] I/protractor -
[23:34:00] I/protractor - Type <tab> to see a list of locator strategies.
[23:34:00] I/protractor - Use the `list` helper function to find elements by strategy:
[23:34:00] I/protractor -   e.g., list(by.binding('')) gets all bindings.
[23:34:00] I/protractor -
Starting debugger agent.
Debugger listening on [::]:5858
> browser.getTitle()
ScriptTimeoutError: Timed out waiting for asynchronous Angular tasks to finish after 11 seconds. This may be because the current page is not an Angular application. Please see the FAQ for more details: https://github.com/angular/protractor/blob/master/docs/timeouts.md#waiting-for-angular.
The following tasks were pending:
 - $timeout: function (){d||(c=new Error("Facebook SDK load timed out [timeout: 60000]"),s.reject(c))}
```


## Part 2


In a perfect world, the goal of end-to-end testing is being able to emulate every path your users will follow and ensuring every piece functions well together. Practically, I see e2e being most useful in ensuring all the happy paths are working as they should.

With this in mind I would start off drawing a map from the different ways a user can land on the site to every step that lead towards the end-goal of them making a purchase on the site. After that, connecting all these nodes and determining which ones are most important dependent on various business metrics. Possibly most common or most profitable paths.

Now that we have our happy paths determined, we need to test them. Our user needs a hotel for tonight, his wife put him in the dog house.

Our user, Fido, wants the most points he can get for under $150 in Chicago. One night should be enough.

* Fido loads the home page.
* Enters in what he wants for a hotel.
* Lands on the listing page.
* Applies filters.
* Selects a hotel and it opens in a new tab
* Reads about it, looks at pictures, is satisfied, selects a room.
* Fills in all forms on checkout page, submits.

So that's the happy path that needs to be verified that everything works. But, what about the sad path? What are some issues that we could run into that might keep Fido from having a perfect user experience?

* Fido loads the home page.
	* He is on mobile and has to cancel out of two modals. One to download the app, one to register for the site.
* Enters in what he wants for a hotel.
	* He enters it for today one minute before midnight. Submits one minute after midnight. Now he gets a message that says there are no listings available. This isn't good! It's the middle of the night and he wants to give us money, but he can't. He can only go back and search for a hotel for the next night.
* Lands on the listing page.
* Applies filters.
	* Price filter button is too small and he gets frustrated!
	* [screenshot](images/listing_screenshot.png) 
* Selects a hotel and it opens in a new tab
	* He doesn't like this one! Can't press the back button on desktop and can't swipe back on mobile.
	* After a couple extra movements, gets out alive.
* Reads about it, looks at pictures, is satisfied, selects a room.  
* Fills in all forms on checkout page
	* Dilligently examines the checkout page because he is worried about his credit card being stolen.
	* Sees weird chinese characters at the bottom of the page and backs out because he thinks rocketmiles has been hacked.
	* ![Hackers!](images/footer_screenshot.png)

	
After discovering these issues on the rocketmiles site, I would write failing tests for all these functional issues while in communication with the proper party to get them corrected.